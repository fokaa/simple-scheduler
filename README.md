# README #

A very simple tasks scheduler for node.js backed by mongo. I wrote this small utility as I needed only a small subset of the functionality other packages provide.

The scheduler saves jobs to a mongo DB called SimpleScheduler and tries to restore the jobs in case it gets reconnected.

v. 0.1.3

### Installation ###

* clone the repository 

### Usage ###

```javascript

const Scheduler = require('./simple-scheduler/lib');

const sch = new Scheduler("sname");

try {
    sch.connect();
} catch (error) {
    process.exit();
}


const callback = function(name) {
    console.log(`Called with argument ${name}`);
} 

try {
    sch.addJob("id1", 60000, callback, "simple-scheduler");
} catch (error) {
    console.error(`Add job finished with ${error}`)
}


```
   
### Reference ###
* constructor Scheduler(id, (optional) mongoConfig, (optional) debug)
    * id - String - scheduler id, must be unique
    * mongoConfig - Dictionary
        * {
        *   host: (default: "localhost"),
        *   port: (default: 27017),
        *   login: (default: ""),
        *   password: (default: "")
        * }  
* connect() - connect to mongo and restore jobs for the scheduler with id, returns Promise
* disconnect() - disconnect from mongo and remove all pending timers, returns Promise
* addJob(id, offset, callback, arg) - add new job to mongo and setup timer, returns Promise
    * id - must be unique,
    * offset - milliseconds from now,
    * callback - callback function,
    * arg - argument to callback
* getJob(id) - get job information, returns Promise
* getJobs() - get list of jobs, returns Promise
* removeJob(id) - remove job, returns Promise
* clean() - remove all jobs from mongo and all timers, returns Promise
* restore() - restore timers from mongo, returns Promise
 
### License ###

The MIT License
